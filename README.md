# Kubernetes

O técnico dos containers:

https://medium.com/trainingcenter/kubernetes-o-t%C3%A9cnico-dos-containers-dffe574c276a

![Defautl Backend](https://cdn-images-1.medium.com/max/2400/0*N8GEyFs7CFLnU9AK.jpg)

<h2>Arquitetura<h2>

![Defautl Backend](https://assets.website-files.com/58de5a002efbcf655ab4677e/5c6edf4140fe314e12bfc926_graphs.jpg)


# Instalação 

Em cada nó:

Faça sempre todos os comandos como root:

```
sudo su
``` 
# Instalação do docker (Todos os nós, incluindo o master)

```
curl -fsSL https://get.docker.com | bash
```
# Instalação do Kubernetes (Todos os nós, incluindo o master)

```
apt-get update && apt-get install -y apt-transport-https curl
```
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
```
```
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
```
```
apt-get update
```
```
apt-get install -y kubelet kubeadm kubectl
```
```
apt-mark hold kubelet kubeadm kubectl
```


`Explicação rápida`

Kubelet: Passa as informações do que está acontecendo com os containers e nós o para o Kubernetes API

Kubeadm: Inicializa e cria o cluster

Kubectl: CLI do kubernetes. Linha de comando.



# Configurações (Todos os nós, incluindo o master)

Desligar swap:

```
swapoff -a 
```

Alterar Cgroups:

Control Groups: Isolamento de recursos
https://access.redhat.com/documentation/pt-br/red_hat_enterprise_linux/6/html/resource_management_guide/ch01

```
docker info | grep Cgroup
```
Retorno:

```
Cgroup Driver: cgroupfs
WARNING: No swap limit support
```


Crie o arquivo daemon.json: 

```
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
```


Crie o diretório: 
```
mkdir -p /etc/systemd/system/docker.service.d
```
Reinicie o docker: 
```
systemctl daemon-reload
```
```
systemctl restart docker
```


Verifique novamente o Cgroup driver: 

```
docker info | grep Cgroup
```

Resultado:
```
Cgroup Driver: systemd
WARNING: No swap limit support
```

# Inicializando o cluster (Nó master)


Vamos utilizar o node1 como master.

```
kubeadm config images pull
```

```
Explicação sobre alguns componentes:

Api server (escreve as cinfugurações, estado no etcd). Principal componente.
Scheduller: define para onde  (Nó) vai cada pod
Proxy: Criar e gerenciar as regras de exposição  dos pods 
***********
Etcd:
***********
Coredns:
```

Inicializar o cluster:

kubeadm init

Executar::

```
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Um último passo:

Pod network: comunicação entre os pods (nós)

```
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 |  tr -d '\n')"
```

# Adição de nós ao cluster (Todos os nós, exceto o master)

Adicionar nós no cluster:

Exemplo:

kubeadm join 10.142.0.2:6443 --token aoxcn0.bvj6zexz5zgemxox \
    --discovery-token-ca-cert-hash sha256:5b115de6a7e84ea4fd9a4d4f8d41400df3032a7f5b050a9a2af20c5b2eba47b9


Perdi o comando pra fazer join, e agora?

Calma...no nó master, execute:

sudo kubeadm token create --print-join-command 




# Conceitos e comandos


<h2>Cluster<h2>

![Defautl Backend](https://cdn-images-1.medium.com/max/1600/0*PF9xzcXceXdOWJrC.)

```
kubectl cluster-info
```

<h2>Node<h2>

![Defautl Backend](https://cdn-images-1.medium.com/max/1600/1*tXf4AJSL7QEgJRdiBG2VxQ.png)

Execute no nó master:

```
kubectl get nodes
```
```
kubectl describe node node1
```

<h2>Pod<h2>

Lembram dos containers? ...

```
kubectl get pods -n kube-system
```

<h2>Namespace<h2>

```
kubectl get ns
```

<h2>Executando um container<h2>

```
kubectl run --port 80 nginx --image=nginx
```

Visualizando todos os recursos:

```
kubectl get all
```

<h2>Deployment, ReplicaSet e Service<h2>

![Defautl Backend](https://imgix.datadoghq.com/img/blog/monitoring-kubernetes-era/kubernetes-services.png)



Vamos explorar alguns comandos:


```
kubectl get deployments
```
```
kubectl get pods
```
```
kubectl describe deployments nginx
```
```
kubectl describe replicasets nginx
```
```
kubectl describe replicasets nginx
```

Como acessar o pod? Serviço!!!

```
kubectl get svc
```

```
kubectl expose deployment nginx
```

# Tipos de Serviço

<h1> Cluster IP, Node Port e LoadBalancer<h1>

Definições:

* ClusterIP: Interno dentro do cluster
* Node Port e LoadBalancer: Exposição para dentro do cluster

Vamos acessar o serviço NGINX criado:

``` 
curl http://CLUSTER_IP:PORT
```

Delete o Serviço ClusterIP criado:

``` 
kubectl get svc

kubectl delete svc [nome_do_servico]

```

Depois exponha o serviço através de NodePort:

```
kubectl expose deployment nginx --type=NodePort
```

LoadBalancer: Só funciona se estiver usando um cloud provider, ou então usando um proxy (Traefik, Envoy)


<h1>Editando um serviço<h1>

```
kubectl edit service nginx
```

Range de portas:
30000 - 22767

<h1>Criando um serviço do tipo Load Balancer<h1>

Delete o serviço nginx:

```
kubectl delete service nginx
```

*** Obtenha o IP Externo!

```
kubectl expose deployment nginx --type=LoadBalancer --name=nginx --external-ip=35.227.84.19
```


# YAML

kubectl get deployments nginx -o yaml

kubectl get deployments nginx -o yaml >> deployment_nginx.yaml


# Deploy do nosso aplicativo de exemplo

Acesse a pasta: "cars-api".

Vamos explorar os arquivos...

Faça o deploy da aplicação.



